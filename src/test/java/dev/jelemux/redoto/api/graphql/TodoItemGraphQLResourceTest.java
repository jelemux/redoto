package dev.jelemux.redoto.api.graphql;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.MediaType;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

@QuarkusTest
@Disabled
class TodoItemGraphQLResourceTest {

    @Test
    void testGetTodoListsEndpoint() {
        String request = "{ todoLists { id title done doneDate children{ id title done doneDate } } }";

        post(request)
                .body("data.todoLists.title", CoreMatchers.hasItems("Shopping List", "Work stuff"))
                .log().ifError();
    }

    @Test
    void testGetTodoItemEndpoint() {
        String request = "{ todoItem(id: 3) { id title done doneDate } }";

        post(request)
                .body("data.todoItem.title", CoreMatchers.equalTo("Cheese"))
                .log().ifError();
    }
    
    @Test
    void testDeleteTodoItemEndpoint() {
        String request = "mutation { deleteTodoItem(id: 2) }";

        post(request)
                .body("data.deleteTodoItem", CoreMatchers.equalTo(true))
                .log().ifError();
    }
    
    @Test
    void testAddTodoItemEndpoint() {
        String request = "mutation { addTodoItem(title: \"Another item to buy\", parentId: 1) { id title done doneDate } }";

        post(request)
                .body("data.addTodoItem.title", CoreMatchers.equalTo("Another item to buy"))
                .log().ifError();
    }
    
    @Test
    @Disabled
    void testUpdateTodoItemEndpoint() {
        String jsonBody = "{\"title\":\"Corn Wafers\",\"done\":true}";

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(jsonBody)
                .when().put("9")
                .then().statusCode(200)
                .log().ifError();
    }

    private ValidatableResponse post(String request) {
        return RestAssured.given().when()
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .body(getPayload(request))
                .post("/graphql")
                .then()
                .assertThat()
                .statusCode(200);
    }

    private String getPayload(String query) {
        JsonObject jsonObject = createRequestBody(query);
        return jsonObject.toString();
    }

    private JsonObject createRequestBody(String graphQL) {
        return createRequestBody(graphQL, null);
    }

    private JsonObject createRequestBody(String graphQL, JsonObject variables) {
        // Create the request
        if (variables == null || variables.isEmpty()) {
            variables = Json.createObjectBuilder().build();
        }
        return Json.createObjectBuilder().add("query", graphQL).add("variables", variables).build();
    }


}
