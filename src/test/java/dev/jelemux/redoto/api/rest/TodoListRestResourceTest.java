package dev.jelemux.redoto.api.rest;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;

import dev.jelemux.redoto.core.model.Task;
import dev.jelemux.redoto.core.model.TodoList;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@QuarkusTest
@TestHTTPEndpoint(TodoListRestResource.class)
class TodoListRestResourceTest extends AbstractTodoItemRestResourceTest {

    @Test
    void testGetEndpoint() {
        RestAssured.given()
                .when().get()
                .then().statusCode(200)
                .contentType(ContentType.JSON)
                .body("title", CoreMatchers.hasItem("Shopping List"));
    }
    
    @Test
    void testCreateEndpoint() {
        var body = new TodoList();
        body.setOrderNum(3);
        body.setTitle("Another todo list");

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(body)
                .when().post()
                .then().statusCode(201);
    }

    @Test
    void testGetSingleEndpoint() {
        RestAssured.given()
                .when().get("1")
                .then().statusCode(200)
                .contentType(ContentType.JSON)
                .body("title", CoreMatchers.equalTo("Shopping List"));
    }
    
    @Test
    void testDeleteEndpoint() {
        RestAssured.given()
                .when().delete("2")
                .then().statusCode(204);
    }
    
    @Test
    void testUpdateEndpoint() {
        var body = new TodoList();
        body.setId(1L);
        body.setOrderNum(4); // Order changed
        body.setTitle("Shopping List");

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(body)
                .when().put("1")
                .then().statusCode(200);
    }
    
    @Test
    void testAddTaskEndpoint() {
        var body = new Task();
        body.setDone(true);
        body.setOrderNum(13);
        body.setTitle("Some Item to buy");

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(body)
                .when().post("1")
                .then().statusCode(201);

    }
    
}
