package dev.jelemux.redoto.api.rest;

import org.junit.jupiter.api.Test;

public abstract class AbstractTodoItemRestResourceTest {
    
    @Test
    abstract void testGetEndpoint();

    @Test
    abstract void testCreateEndpoint();

    @Test
    abstract void testGetSingleEndpoint();

    @Test
    abstract void testDeleteEndpoint();
    
    @Test
    abstract void testUpdateEndpoint();

}
