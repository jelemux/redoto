package dev.jelemux.redoto.api.rest;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import dev.jelemux.redoto.core.model.AbstractTodoItem;
import dev.jelemux.redoto.core.services.TodoItemServiceSpecification;
import io.smallrye.mutiny.Uni;
import lombok.NoArgsConstructor;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@NoArgsConstructor
public abstract class AbstractTodoItemRestResource<T extends AbstractTodoItem, S extends TodoItemServiceSpecification<T>> {

    S service;

    AbstractTodoItemRestResource(S service) {
        this.service = service;
    }

    @GET
    public Uni<List<T>> get() {
        return service.get();
    }

    @POST
    public Uni<Response> create(T item) {
        return service.create(item)
                .onItem().transform(i -> {
                    if (i.getId() == null) {
                        return Response.status(Status.INTERNAL_SERVER_ERROR);
                    } else {
                        // TODO enforce annotation in either a Unit test or at build time.
                        var uri = "/" + this.getClass().getAnnotation(Path.class).value() + "/" + i.getId();
                        return Response.created(URI.create(uri));
                    }
                }).onItem().transform(ResponseBuilder::build);
    }

    @GET
    @Path("{id}")
    public Uni<Response> getSingle(Long id) {
        return service.getSingle(id)
                .onItem().transform(item -> item != null ? Response.ok(item) : Response.status(Status.NOT_FOUND))
                .onItem().transform(ResponseBuilder::build);
    }
    
    @DELETE
    @Path("{id}")
    public Uni<Response> delete(Long id) {
        return service.delete(id)
                .onItem().transform(deleted -> Boolean.TRUE.equals(deleted) ? Status.NO_CONTENT : Status.NOT_FOUND)
                .onItem().transform(status -> Response.status(status).build());
    }
    
    @PUT
    @Path("{id}")
    public Uni<Response> update(Long id, T item) {
        item.setId(id);
        return service.update(item)
                .onItem().transform(updated -> Boolean.TRUE.equals(updated) ? Status.OK : Status.NOT_FOUND)
                .onItem().transform(status -> Response.status(status).build());
    }
    
}
