package dev.jelemux.redoto.api.rest;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Path;

import dev.jelemux.redoto.core.model.SubTask;
import dev.jelemux.redoto.core.services.SubTaskService;

@Path("subTasks")
@ApplicationScoped
public class SubTaskRestResource extends AbstractTodoItemRestResource<SubTask, SubTaskService> {

    @Inject 
    public SubTaskRestResource(SubTaskService service) {
        super(service);
    }
    
}
