package dev.jelemux.redoto.api.rest;

import java.net.URI;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import dev.jelemux.redoto.core.model.Task;
import dev.jelemux.redoto.core.model.TodoList;
import dev.jelemux.redoto.core.services.TodoListService;
import io.smallrye.mutiny.Uni;

@Path("todoLists")
@ApplicationScoped
public class TodoListRestResource extends AbstractTodoItemRestResource<TodoList, TodoListService> {
    
    @Inject
    public TodoListRestResource(TodoListService service) {
        super(service);
    }

    @POST
    @Path("{listId}")
    public Uni<Response> addTask(Long listId, Task task) {
        return service.addTask(listId, task)
                .onItem().transform(t -> {
                    if (t.getId() == null) {
                        return Response.status(Status.INTERNAL_SERVER_ERROR);
                    } else {
                        var uri = URI.create("/tasks/" + t.getId());
                        return Response.created(uri);
                    }
                }).onItem().transform(ResponseBuilder::build);
    }

}
