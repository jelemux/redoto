package dev.jelemux.redoto.api.rest;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import dev.jelemux.redoto.core.model.SubTask;
import dev.jelemux.redoto.core.model.Task;
import dev.jelemux.redoto.core.services.TaskService;
import io.smallrye.mutiny.Uni;

@Path("tasks")
@ApplicationScoped
public class TaskRestResource extends AbstractTodoItemRestResource<Task, TaskService> {
    
    @Inject
    public TaskRestResource(TaskService service) {
        super(service);
    }

    @POST
    @Path("{taskId}")
    public Uni<SubTask> addSubTask(Long taskId, SubTask subTask) {
        return service.addSubTask(taskId, subTask);
    }

}
