package dev.jelemux.redoto.api.graphql;

import org.eclipse.microprofile.graphql.GraphQLApi;

@GraphQLApi
public class TodoItemGraphQLResource {

    /*
    // FIXME IllegalStateException when supplying null as an id

    @Inject
    TodoItemServiceSpecification service;
    
    @Query
    @Description("Get all todo lists.")
    public Uni<List<AbstractTodoItem>> getTodoLists() {
        return service.getTodoLists();
    }

    @Query
    @Description("Get a item of a todo list.")
    public Uni<AbstractTodoItem> getTodoItem(@NonNull Long id) {
        return service.getTodoItem(id);
    }

    @Mutation
    @Description("Permanently deletes a todo item or list. Returns true if successful.")
    public Uni<Boolean> deleteTodoItem(@NonNull Long id) {
        return service.deleteTodoItem(id);
    }

    @Mutation
    @Description("Adds a todo item to another todo item. Creates a todo list if parentId is null.")
    public Uni<AbstractTodoItem> addTodoItem(Long parentId, @NonNull String title, Boolean done) {
        var item = new AbstractTodoItem(title);
        item.setDone(done);
        return service.addTodoItem(parentId, item);
    }

    @Mutation
    @Description("Updates a todo item.")
    public Uni<Boolean> updateTodoItem(@NonNull AbstractTodoItem item) {
        return service.updateTodoItem(item);
    }
    */

}
