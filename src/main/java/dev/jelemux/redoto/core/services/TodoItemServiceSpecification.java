package dev.jelemux.redoto.core.services;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import dev.jelemux.redoto.core.model.AbstractTodoItem;
import io.smallrye.mutiny.Uni;

public interface TodoItemServiceSpecification<T extends AbstractTodoItem> {
    
    /**
     * Gets all items of this type.
     * 
     * @return items of this type
     */
    public Uni<List<T>> get();

    /**
     * Gets a single item by id.
     * 
     * @param id id of the item to return
     * @return specified item
     */
    public Uni<T> getSingle(@NotNull Long id);

    /**
     * Creates an item of this type.  
     * 
     * @param item item to create
     * @return created item
     */
    @Transactional
    public Uni<T> create(@Valid T item);

    /**
     * Deletes an item by id.  
     * 
     * @param id id of the item to delete
     * @return true if successful
     */
    @Transactional
    public Uni<Boolean> delete(@NotNull Long id);

    /**
     * Updates the item with the provided information.  
     * Id must be set to know which item to update.  
     * This is a PUT operation.  
     * 
     * @param item the item to update
     * @return true if successful
     */
    @Transactional
    public Uni<Boolean> update(@Valid T item);
    
}
