package dev.jelemux.redoto.core.services;

import java.util.List;

import javax.enterprise.context.Dependent;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import dev.jelemux.redoto.core.model.SubTask;
import io.smallrye.mutiny.Uni;

@Dependent
public class SubTaskService implements TodoItemServiceSpecification<SubTask> {

    @Override
    public Uni<List<SubTask>> get() {
        return SubTask.listAll();
    }

    @Override
    public Uni<SubTask> getSingle(@NotNull Long id) {
        return SubTask.findById(id);
    }

    @Override
    public Uni<SubTask> create(@Valid SubTask item) {
        item.persistAndFlush();
        return Uni.createFrom().item(item);
    }

    @Override
    public Uni<Boolean> delete(@NotNull Long id) {
        return SubTask.deleteById(id);
    }

    @Override
    public Uni<Boolean> update(@Valid SubTask item) {
        return Uni.createFrom().item(item.isPersistent());
    }
    
}
