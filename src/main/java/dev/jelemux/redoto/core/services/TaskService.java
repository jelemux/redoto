package dev.jelemux.redoto.core.services;

import java.util.List;

import javax.enterprise.context.Dependent;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import dev.jelemux.redoto.core.model.SubTask;
import dev.jelemux.redoto.core.model.Task;
import io.smallrye.mutiny.Uni;

@Dependent
public class TaskService implements TodoItemServiceSpecification<Task> {

    @Override
    public Uni<List<Task>> get() {
        return Task.listAll();
    }

    @Override
    public Uni<Task> getSingle(@NotNull Long id) {
        return Task.findById(id);
    }

    @Override
    public Uni<Task> create(@Valid Task item) {
        item.persistAndFlush();
        return Uni.createFrom().item(item);
    }

    @Override
    public Uni<Boolean> delete(@NotNull Long id) {
        return Task.deleteById(id);
    }

    @Override
    public Uni<Boolean> update(@Valid Task item) {
        return Uni.createFrom().item(item.isPersistent());
    }

    public Uni<SubTask> addSubTask(@NotNull Long taskId, @Valid @NotNull SubTask subTask) {
        return Task.findById(taskId)
                .onItem().transform(t -> {
                    Task task = ((Task) t);
                    task.addSubTask(subTask);
                    task.persistAndFlush();

                    return subTask;
                });
    }
    
}
