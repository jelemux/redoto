package dev.jelemux.redoto.core.services;

import java.util.List;

import javax.enterprise.context.Dependent;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import dev.jelemux.redoto.core.model.Task;
import dev.jelemux.redoto.core.model.TodoList;
import io.smallrye.mutiny.Uni;

@Dependent
public class TodoListService implements TodoItemServiceSpecification<TodoList> {

    @Override
    public Uni<List<TodoList>> get() {
        return TodoList.listAll();
    }

    @Override
    public Uni<TodoList> getSingle(@NotNull Long id) {
        return TodoList.findById(id);
    }

    @Override
    public Uni<TodoList> create(@Valid TodoList item) {
        item.persistAndFlush();
        return Uni.createFrom().item(item);
    }

    @Override
    public Uni<Boolean> delete(@NotNull Long id) {
        return TodoList.deleteById(id);
    }

    @Override
    public Uni<Boolean> update(@Valid TodoList item) {
        return Uni.createFrom().item(item.isPersistent());
    }

    public Uni<Task> addTask(@NotNull Long listId, @Valid @NotNull Task task) {
        return TodoList.findById(listId)
                .onItem().transform(l -> {
                    TodoList list = ((TodoList) l);
                    list.addTask(task);
                    list.persistAndFlush();

                    return task;
                });
    }
    
}
