package dev.jelemux.redoto.core.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Task extends AbstractTask {
    
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<SubTask> subTasks = new ArrayList<>();

    public void addSubTask(SubTask subTask) {
        this.subTasks.add(subTask);
    }

    @Override
    protected void doneChangedAction() {
        super.doneChangedAction();

        if (Boolean.TRUE.equals(this.getDone())) {
            this.subTasks.stream().forEach(st -> st.setDone(true));
        }
    }

}
