package dev.jelemux.redoto.core.model;

import java.time.LocalDate;

import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
class AbstractTask extends AbstractTodoItem {

    @NotNull
    private Boolean done = false;

    private LocalDate doneDate;
    
    public void setDone(Boolean done) {
        if (done == null) {
            this.done = false;
        } else {
            this.done = done;
        }
        doneChangedAction();
    }

    protected void doneChangedAction() {
        if (Boolean.TRUE.equals(this.done)) {
            this.doneDate = LocalDate.now();
        } else {
            this.doneDate = null;
        }
    }
}
