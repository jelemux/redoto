package dev.jelemux.redoto.core.model;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import lombok.Getter;
import lombok.Setter;

/**
 * Entry in a "cool" and "hip" Todo List.
 */
@Getter
@Setter
@MappedSuperclass
public abstract class AbstractTodoItem extends PanacheEntityBase {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private Integer orderNum;

    @NotNull
    private String title;

    protected AbstractTodoItem() {
    }
    
    protected AbstractTodoItem(String title) {
        this.title = title;
    }

}
