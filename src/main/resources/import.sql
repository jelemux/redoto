/* Shopping List */
INSERT INTO TodoList ( id, orderNum, title ) VALUES ( 1, 1, 'Shopping List' );
INSERT INTO Task ( id, orderNum, title, done, doneDate ) VALUES ( 1, 2, 'Cheese',         true,  '2021-03-24' );
INSERT INTO Task ( id, orderNum, title, done, doneDate ) VALUES ( 2, 1, 'Apples',         true,  '2021-03-24' );
INSERT INTO Task ( id, orderNum, title, done, doneDate ) VALUES ( 3, 3, 'Chia Seeds',     true,  '2021-03-24' );
INSERT INTO Task ( id, orderNum, title, done, doneDate ) VALUES ( 4, 4, 'Rice Drinks 3x', true,  '2021-03-24' );
INSERT INTO Task ( id, orderNum, title, done, doneDate ) VALUES ( 5, 5, 'Oat Flakes',     true,  '2021-03-24' );
INSERT INTO Task ( id, orderNum, title, done, doneDate ) VALUES ( 6, 6, 'Honey Spelt',    true,  '2021-03-24' );
INSERT INTO Task ( id, orderNum, title, done, doneDate ) VALUES ( 7, 7, 'Linseed',        true,  '2021-03-24' );
INSERT INTO Task ( id, orderNum, title, done, doneDate ) VALUES ( 8, 8, 'Corn Wafers',    false, null         );
INSERT INTO Task ( id, orderNum, title, done, doneDate ) VALUES ( 9, 9, 'Cream Cheese',   false, null         );
INSERT INTO TodoList_Task ( TodoList_id, tasks_id ) VALUES ( 1,  2 );
INSERT INTO TodoList_Task ( TodoList_id, tasks_id ) VALUES ( 1,  3 );
INSERT INTO TodoList_Task ( TodoList_id, tasks_id ) VALUES ( 1,  4 );
INSERT INTO TodoList_Task ( TodoList_id, tasks_id ) VALUES ( 1,  5 );
INSERT INTO TodoList_Task ( TodoList_id, tasks_id ) VALUES ( 1,  6 );
INSERT INTO TodoList_Task ( TodoList_id, tasks_id ) VALUES ( 1,  7 );
INSERT INTO TodoList_Task ( TodoList_id, tasks_id ) VALUES ( 1,  8 );
INSERT INTO TodoList_Task ( TodoList_id, tasks_id ) VALUES ( 1,  9 );
INSERT INTO TodoList_Task ( TodoList_id, tasks_id ) VALUES ( 1, 10 );

/* Work Todo List */
INSERT INTO TodoList ( id, orderNum, title ) VALUES ( 2, 2, 'Work stuff' );
INSERT INTO Task ( id, orderNum, title, done, doneDate ) VALUES ( 10, 10, 'Check out Quarkus',                 true, '2021-05-18' );
INSERT INTO Task ( id, orderNum, title, done, doneDate ) VALUES ( 11, 11, 'Build an example app with Quarkus', false, null        );
INSERT INTO Task ( id, orderNum, title, done, doneDate ) VALUES ( 12, 12, 'Find a good Frontend Framework',    false, null        );
INSERT INTO TodoList_Task ( TodoList_id, tasks_id ) VALUES ( 2, 10 );
INSERT INTO TodoList_Task ( TodoList_id, tasks_id ) VALUES ( 2, 11 );
INSERT INTO TodoList_Task ( TodoList_id, tasks_id ) VALUES ( 2, 12 );
INSERT INTO SubTask ( id, orderNum, title, done, doneDate ) VALUES ( 1, 1, 'Build Backend',  false, null );
INSERT INTO SubTask ( id, orderNum, title, done, doneDate ) VALUES ( 2, 2, 'Add security',   false, null );
INSERT INTO SubTask ( id, orderNum, title, done, doneDate ) VALUES ( 3, 3, 'Build Frontend', false, null );
INSERT INTO Task_SubTask ( Task_id, subTasks_id ) VALUES ( 11, 1 );
INSERT INTO Task_SubTask ( Task_id, subTasks_id ) VALUES ( 11, 2 );
INSERT INTO Task_SubTask ( Task_id, subTasks_id ) VALUES ( 11, 3 );